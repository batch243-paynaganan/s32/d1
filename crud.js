let http = require('http')
let post = 4000

// mock database
let directory = [
    {"name":"Brandon",
     'email':'brandon@mail.com'},
    {'name':'Jobert',
     'email':"jobert@mail.com"}
]
const server=http.createServer((req, res)=>{
    if(req.url=='/users' && req.method=='GET'){
        res.writeHead(200,{'Content-Type':'application/json'});
        res.write(JSON.stringify(directory));
        res.end();
    }if(req.url=='/users' && req.method=='POST'){
        let requestBody = "";
        req.on('data',function(data){
            requestBody += data;
        });
        req.on('end', function(){
            console.log(typeof requestBody);
            requestBody = JSON.parse(requestBody);
            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }
            // add the new user into the database by using the push method
            directory.push(newUser)
            console.log(directory)

            res.writeHead(200,{'Content-Type':'application/json'})
            res.write(JSON.stringify(newUser))
            res.end();
        })
    }
}).listen(4000)

console.log(`Server running at localhost: ${post}`)